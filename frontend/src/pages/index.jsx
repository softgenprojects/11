import { useState, useEffect } from 'react';
import { Box, VStack, Heading, Center, useColorModeValue } from '@chakra-ui/react';
import { ConfettiComponent } from '@components/ConfettiComponent';

const Home = () => {
  const [isConfettiActive, setIsConfettiActive] = useState(false);
  const bgColor = useColorModeValue('white', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  useEffect(() => {
    let timer;
    if (typeof window !== 'undefined') {
      setIsConfettiActive(true);
      timer = setTimeout(() => setIsConfettiActive(false), 5000);
    }
    return () => {
      if (timer) clearTimeout(timer);
    };
  }, []);

  return (
    <Box bg={bgColor} color={textColor} minH="100vh" w="full">
      <Center h="100vh" flexDirection="column" justifyContent="center" p={{ base: '4', lg: '8' }}>
        <VStack spacing="8">
          <Heading as="h1" size="4xl" fontWeight="bold" textAlign="center" letterSpacing="tight">
            11s the charm baby
          </Heading>
        </VStack>
      </Center>
      {isConfettiActive && <ConfettiComponent isActive={isConfettiActive} />}
    </Box>
  );
};

export default Home;