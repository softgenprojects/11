import React from 'react';
import Confetti from 'react-confetti';
import { useWindowSize } from '@hooks/useWindowSize';

export const ConfettiComponent = ({ isActive }) => {
  const { width, height } = useWindowSize();

  return isActive && width !== null && height !== null ? <Confetti width={width} height={height} /> : null;
};