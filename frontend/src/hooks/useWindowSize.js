import { useState, useEffect } from 'react';

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState({ width: null, height: null });
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const handleResize = () => {
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight
        });
      };

      window.addEventListener('resize', handleResize);
      handleResize();
      setIsMounted(true);

      return () => window.removeEventListener('resize', handleResize);
    }
  }, []);

  return isMounted ? windowSize : { width: null, height: null };
};