const { getRecommendations } = require('@services/RecommendationService');

async function recommendationsHandler(req, res) {
  const userId = req.user.id;
  const recommendations = await getRecommendations(userId);
  res.json(recommendations);
}

module.exports = { recommendationsHandler };