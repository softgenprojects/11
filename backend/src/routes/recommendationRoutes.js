const express = require('express');
const { recommendationsHandler } = require('@controllers/RecommendationController');
const { authenticationMiddleware } = require('@middleware/authenticationMiddleware');

const router = express.Router();

router.get('/topic/recommendations', authenticationMiddleware, recommendationsHandler);

module.exports = router;